<?php

/**
 * @file
 * Contains views_input_required_all_exposed_form_plugin.
 */

/**
 * Class views_input_required_all_exposed_form_plugin.
 */
class views_input_required_all_exposed_form_plugin extends views_plugin_exposed_form_input_required {

  /**
   * {@inheritdoc}
   */
  function exposed_filter_applied() {
    static $cache;

    if (!isset($cache)) {
      $cache = TRUE;
      $view = $this->view;
      if (is_array($view->filter) && count($view->filter)) {
        foreach ($view->filter as $filter_id => $filter) {
          if ($filter->is_exposed()) {
            $id = $filter->options['expose']['identifier'];
            if (!isset($view->exposed_input[$id]) || $view->exposed_input[$id] == 'All') {
              $cache = FALSE;
              return $cache;
            }
          }
        }
      }

    }

    return $cache;
  }

}
