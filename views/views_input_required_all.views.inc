<?php

/**
 * @file
 * Contains Views functionality.
 */

/**
 * Implements hook_views_plugins().
 */
function views_input_required_all_views_plugins() {
  return array(
    'exposed_form' => array(
      'views_input_required_all' => array(
        'title' => t('Input required for all exposed filters'),
        'help' => t('An exposed form that only renders view rows if all form elements contains user input'),
        'handler' => 'views_input_required_all_exposed_form_plugin',
        'uses options' => TRUE,
        'type' => 'normal',
        'parent' => 'input_required',
      ),
    ),
  );
}
