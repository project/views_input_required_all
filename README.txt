Require all Exposed Filters

Provides an exposed form that only renders view rows if all form elements
contains user input. The module is similar to standard Views exposed form
"Input required" but requires that the user has set an explicit ("-Any-" is not
considered explicit) value to all exposed filters.

Author: Claudiu Cristea | https://www.drupal.org/u/claudiu.cristea
